package me.FinestDev.CastleDefense;

import me.FinestDev.CastleDefense.Commands.Commands;
import me.FinestDev.CastleDefense.Compass.CompassClass;
import me.FinestDev.CastleDefense.Events.EventsMisc;
import me.FinestDev.CastleDefense.Events.PlayerBlockEvent;
import me.FinestDev.CastleDefense.Events.PlayerDamage;
import me.FinestDev.CastleDefense.Events.PlayerJoin;
import me.FinestDev.CastleDefense.Events.PlayerLeave;
import me.FinestDev.CastleDefense.Events.SpectatorInteract;
import me.FinestDev.CastleDefense.Game.GameStart;
import me.FinestDev.CastleDefense.Game.GameStop;
import me.FinestDev.CastleDefense.Items.Items;
import me.FinestDev.CastleDefense.Kits.KitGUI;
import me.FinestDev.CastleDefense.Kits.KitNebulanaut;
import me.FinestDev.CastleDefense.Kits.KitNebulax;
import me.FinestDev.CastleDefense.Kits.KitWizard;
import me.FinestDev.CastleDefense.Kits.Kits;
import me.FinestDev.CastleDefense.Timers.GameTimer;
import me.FinestDev.CastleDefense.Utilities.Config;
import me.FinestDev.CastleDefense.Utilities.ScoreboardClass;
import me.FinestDev.CastleDefense.Utilities.Util;
import me.FinestDev.CastleDefense.Villagers.KillVillager;
import me.FinestDev.CastleDefense.Villagers.SpawnVillagers;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

	public Config config = new Config(this);
	public GameStart gamestart = new GameStart(this);
	public GameTimer gametimer = new GameTimer(this);
	public GameStop gamestop = new GameStop(this);
	public SpawnVillagers spawnvillagers = new SpawnVillagers(this);
	public ScoreboardClass scoreboard = new ScoreboardClass(this);
	public CompassClass compassclass = new CompassClass(this);

	public void onEnable(){
		
		
		saveDefaultConfig();
		
		Util.gameWorld = getConfig().getString("Game.Locations.GameWorld");

		Util.loadWorld();
		
		// Setup our config variables
		config.setupConfig();
		Kits.kitMenu = Bukkit.createInventory(null, 9, Util.menuName);
		Items.setupItems();
		
		// Setup our command
		getCommand("startchallenge").setExecutor(new Commands(this));
		getCommand("castledefense").setExecutor(new Commands(this));
		
		// Register our listeners
		Bukkit.getPluginManager().registerEvents(new PlayerJoin(this), this);
		Bukkit.getPluginManager().registerEvents(new PlayerLeave(this), this);
		Bukkit.getPluginManager().registerEvents(new PlayerDamage(this), this);
		Bukkit.getPluginManager().registerEvents(new PlayerBlockEvent(this), this);
		Bukkit.getPluginManager().registerEvents(new KillVillager(this), this);
		Bukkit.getPluginManager().registerEvents(new SpectatorInteract(this), this);
		
		Bukkit.getPluginManager().registerEvents(new EventsMisc(this), this);
		
		Bukkit.getPluginManager().registerEvents(new KitNebulanaut(this), this);
		Bukkit.getPluginManager().registerEvents(new KitWizard(this), this);
		Bukkit.getPluginManager().registerEvents(new KitNebulax(this), this);
		Bukkit.getPluginManager().registerEvents(new KitGUI(this), this);
		
	}

	public void onDisable(){

		if(Util.gameInProgress){
			gamestop.gameStop();
		}
		
		saveConfig();
		
		Util.unloadWorld();
		

	}

}
