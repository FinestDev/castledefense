package me.FinestDev.CastleDefense.Events;

import me.FinestDev.CastleDefense.Main;
import me.FinestDev.CastleDefense.Kits.Kits;
import me.FinestDev.CastleDefense.Utilities.Util;
import me.FinestDev.CastleDefense.Villagers.SpawnVillagers;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PlayerDamage implements Listener {

	Main pl;
	public PlayerDamage(Main in){
		pl = in;
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onDMG(EntityDamageByEntityEvent e){
		if(Util.gameInProgress){
			if(e.getEntity() instanceof Player && e.getDamager() instanceof Player){
				Player attacker = (Player)e.getDamager();
				Player hit = (Player)e.getEntity();

				// Cancel event if they're on the same team
				if(Util.redTeam.contains(attacker.getName()) && Util.redTeam.contains(hit.getName()) ||
						Util.blueTeam.contains(attacker.getName()) && Util.blueTeam.contains(hit.getName())){

					e.setDamage(0.0); // Just in case!
					e.setCancelled(true);

				}
			}
		}else{
			// Lobby Phase
			e.setDamage(0.0);
			e.setCancelled(true);
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onArrow(EntityDamageByEntityEvent event){
		if(event.getDamager() instanceof Arrow){ // An Arrow
			Arrow arrow = (Arrow) event.getDamager();

			if(arrow.getShooter() instanceof Player && event.getEntity() instanceof Player){
				Player shooter = (Player) arrow.getShooter();
				Player hit = (Player) event.getEntity();

				if(Util.redTeam.contains(hit.getName()) && Util.redTeam.contains(shooter.getName())){ // Both on red
					event.setDamage(0.0);
					event.setCancelled(true);
				}
				
				if(Util.blueTeam.contains(hit.getName()) && Util.blueTeam.contains(shooter.getName())){ // Both on blue
					event.setDamage(0.0);
					event.setCancelled(true);
				}

			}
		}
	}

	@EventHandler
	public void onDeath(PlayerDeathEvent e){
		if(e.getEntity() instanceof Player){
			Player p = (Player) e.getEntity();
			if(Util.gameInProgress){
				e.setDeathMessage(null);
				e.getDrops().clear();
				if(Util.blueTeam.contains(p.getName())){
					if(SpawnVillagers.bv.isDead()){
						p.teleport(Util.lobbySpawn);
						Util.removeTeam(p);
					}
				}else if(Util.redTeam.contains(p.getName())){
					if(SpawnVillagers.rv.isDead()){
						p.teleport(Util.lobbySpawn);
						Util.removeTeam(p);
					}
				}
			}
		}
	}

	@EventHandler
	public void onResp(PlayerRespawnEvent e){
		final Player p = e.getPlayer();
		Bukkit.getScheduler().scheduleSyncDelayedTask(pl, new Runnable() {
			public void run(){
				if(Util.gameInProgress){

					if(Util.blueTeam.contains(p.getName())){
						if(!SpawnVillagers.bv.isDead()){
							Util.teleportPlayer(p, Util.blueSpawn);
							Kits.equipKit(p, "Blue");
						}else{
							makeSpec(p);
						}
					}else if(Util.redTeam.contains(p.getName())){
						if(!SpawnVillagers.rv.isDead()){
							Util.teleportPlayer(p, Util.redSpawn);
							Kits.equipKit(p, "Red");
						}else{
							makeSpec(p);
						}
					}
				}else{
					Util.teleportPlayer(p, Util.lobbySpawn);
				}
			}
		},5L);
	}

	public static void makeSpec(Player p){
		Kits.removeKit(p);
		Util.removeTeam(p);
		Util.addToSpec(p);
		Util.teleportPlayer(p, Util.specSpawn);
		p.setGameMode(GameMode.CREATIVE);
		p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1));
	}


}
