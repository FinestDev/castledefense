package me.FinestDev.CastleDefense.Events;

import java.util.ArrayList;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

import me.FinestDev.CastleDefense.Main;

public class SpectatorInteract implements Listener{

	Main pl;
	public SpectatorInteract(Main in){
		pl = in;
	}
	
	public static ArrayList<String> specTeam = new ArrayList<String>();

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onHit(EntityDamageByEntityEvent e){
		
		if(e.getEntity() instanceof Player){
			Player hitter = (Player) e.getEntity();
			
			if(specTeam.contains(hitter.getName())){
				e.setDamage(0.0);
				e.setCancelled(true);
			}
		}
		
		if(e.getDamager() instanceof Player){
			Player damager = (Player) e.getDamager();
			
			if(specTeam.contains(damager.getName())){
				e.setDamage(0.0);
				e.setCancelled(true);
			}
		}
		
	}
	
	@EventHandler
	public void onDMG(EntityDamageEvent e){
		if(e.getEntity() instanceof Player){
			Player p = (Player) e.getEntity();
			
			if(specTeam.contains(p.getName())){
				e.setDamage(0.0);
				e.setCancelled(true);
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onInteract(PlayerInteractEvent e){
		if(specTeam.contains(e.getPlayer().getName())){
			e.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onDrop(PlayerDropItemEvent e){
		if(specTeam.contains(e.getPlayer().getName())){
			e.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onPickUp(PlayerPickupItemEvent e){
		if(specTeam.contains(e.getPlayer().getName())){
			e.setCancelled(true);
		}
	}

}
