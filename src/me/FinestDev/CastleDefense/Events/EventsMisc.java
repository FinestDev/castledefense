package me.FinestDev.CastleDefense.Events;

import me.FinestDev.CastleDefense.Main;
import me.FinestDev.CastleDefense.Utilities.Util;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerDropItemEvent;

public class EventsMisc implements Listener{

	Main pl;
	public EventsMisc(Main in){
		pl = in;
	}
	
	@EventHandler
	public void onRemoveArmor(InventoryClickEvent e){
		if(Util.gameInProgress){
			if(e.getSlotType() == InventoryType.SlotType.ARMOR){
				e.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onDrop(PlayerDropItemEvent e){
		if(Util.gameInProgress){
			if(Util.redTeam.contains(e.getPlayer().getName()) ||
					Util.blueTeam.contains(e.getPlayer().getName())){
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onHunger(FoodLevelChangeEvent e){
		if(pl.getConfig().getBoolean("Game.Hunger") == false){
			e.setCancelled(true);
		}
	}

}
