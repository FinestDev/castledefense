package me.FinestDev.CastleDefense.Events;

import me.FinestDev.CastleDefense.Main;
import me.FinestDev.CastleDefense.Utilities.Util;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerLeave implements Listener {
	
	Main pl;
	public PlayerLeave(Main in){
		pl = in;
	}
	
	@EventHandler
	public void onLeave(PlayerQuitEvent e){
		Player p = e.getPlayer();
		e.setQuitMessage(null);
		
		if(Util.gameInProgress){
			
			// Remove their team
			Util.removeTeam(p);
			p.setAllowFlight(false);
			
		}
		
		if(p.getScoreboard() == pl.scoreboard.board){
			pl.scoreboard.removeScoreBoard(p);
		}
		
	}
}
