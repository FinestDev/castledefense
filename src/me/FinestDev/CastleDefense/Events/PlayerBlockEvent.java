package me.FinestDev.CastleDefense.Events;

import me.FinestDev.CastleDefense.Main;
import me.FinestDev.CastleDefense.Utilities.Util;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class PlayerBlockEvent implements Listener {

	Main pl;
	public PlayerBlockEvent(Main in){
		pl = in;
	}

	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onBreak(BlockBreakEvent e){

		if(Util.gameInProgress){
			Block b = e.getBlock();
			b.getDrops().clear();
			if(b.getType() == Material.WOOL){
				if(b.getData() != (byte) 11 && b.getData() != (byte) 14){
					e.setCancelled(true);
				}else
					if(e.getPlayer().getItemInHand().getType() != Material.SHEARS){
						e.setCancelled(true);
					}

			}else{
				e.setCancelled(true);
			}
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlace(BlockPlaceEvent e){

		if(Util.gameInProgress){
			Block b = e.getBlock();

			if(b.getType() == Material.WOOL){

				if(b.getData() == (byte) 11){
					if(Util.blueTeam.contains(e.getPlayer().getName())){
						return;
					}else{
						e.setCancelled(true);
					}
				}
				else if(b.getData() == (byte) 14){
					if(Util.redTeam.contains(e.getPlayer().getName())){
						return;
					}else{
						e.setCancelled(true);
					}
				}else{
					e.setCancelled(true);
				}

			}else{
				e.setCancelled(true);
			}
		}
	}
}
