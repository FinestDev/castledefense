package me.FinestDev.CastleDefense.Events;

import me.FinestDev.CastleDefense.Main;
import me.FinestDev.CastleDefense.Items.Items;
import me.FinestDev.CastleDefense.Utilities.Util;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PlayerJoin implements Listener {

	Main pl;
	public PlayerJoin(Main in){
		pl = in;
	}

	@EventHandler
	public void onLogin(PlayerLoginEvent e){

		if(Bukkit.getOnlinePlayers().length >= Util.maxPlayers){

			for(Player p : Bukkit.getOnlinePlayers()){
				if(!p.hasPermission("fanchallenge.VIP")){
					p.kickPlayer(ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("Chat.Error.MakeRoom")));
					return;
				}
			}
		}
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		final Player p = e.getPlayer();
		e.setJoinMessage(null);
		
		Util.clearNameTag(p);

		if(p.hasPermission("fanchallenge.member") || p.hasPermission("fanchallenge.vip")){
			
			if(Util.gameInProgress){

				Bukkit.getScheduler().scheduleSyncDelayedTask(pl, new Runnable() {
					public void run(){

						// Make them a spectator
						Util.teleportPlayer(p, Util.specSpawn);
						p.setHealth(20.0);
						p.setGameMode(GameMode.CREATIVE);
						p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1));
						p.getInventory().clear();
						p.getInventory().setArmorContents(null);

						pl.scoreboard.makeScoreBoard(p);
						Util.addToSpec(p);

					}
				},5L);
			}else{

				p.setGameMode(GameMode.SURVIVAL);
				p.setHealth(20.0);
				p.getInventory().clear();
				p.getInventory().setArmorContents(null);
				p.setExp(0);
				p.teleport(Util.lobbySpawn);
				p.getInventory().addItem(Items.menu);

			}
		}else{

			p.kickPlayer(ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("Chat.Error.NotMember")));

		}

	}

}
