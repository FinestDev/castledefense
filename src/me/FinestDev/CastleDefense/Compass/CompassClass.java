package me.FinestDev.CastleDefense.Compass;

import me.FinestDev.CastleDefense.Main;
import me.FinestDev.CastleDefense.Utilities.Util;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class CompassClass {

	Main pl;
	public CompassClass(Main in){
		pl = in;
	}

	private int Task;

	public static String getNearest(Player p){

		String closest = null;
		double distance = 5000;
		for(Player allPlayers : Bukkit.getOnlinePlayers()){

			if(allPlayers != p && !allPlayers.isDead()){

				// Verify that they're an enemy
				if(Util.redTeam.contains(p.getName()) && Util.blueTeam.contains(allPlayers.getName()) ||
						Util.blueTeam.contains(p.getName()) && Util.redTeam.contains(allPlayers.getName())){

					if(allPlayers.getLocation().distance(p.getLocation()) < distance){
						closest = allPlayers.getName();
						distance = Math.round(allPlayers.getLocation().distance(p.getLocation())*100)/100.0D;
					}

				}
			}


		}

		if(closest == null){
			return "None,0.0";
		}else{
			return closest+","+distance;
		}

	}

	public void compassTimer(){

		Task = Bukkit.getScheduler().scheduleSyncRepeatingTask(pl, new Runnable() {

			@SuppressWarnings("deprecation")
			public void run(){

				for(Player p : Bukkit.getOnlinePlayers()){

					if(p.getItemInHand().getType() == Material.COMPASS){

						String nearest = getNearest(p);

						String[] spl = nearest.split(",");
						String target = spl[0];
						String distance = spl[1];

						ItemStack i = p.getItemInHand();

						ItemMeta IM = i.getItemMeta();
						IM.setDisplayName(ChatColor.translateAlternateColorCodes('$', Util.compassMSG.replace("%player%", target).replace("%distance%",
								distance)));
						i.setItemMeta(IM);
						
						p.getInventory().setItemInHand(i);

						if(Bukkit.getPlayer(target) != null){
							p.setCompassTarget(Bukkit.getPlayer(target).getLocation());
						}

						p.updateInventory();

					}

				}

			}

		},20L,20L);

	}

	public void cancelTask(){
		Bukkit.getScheduler().cancelTask(Task);
	}

}
