package me.FinestDev.CastleDefense.Game;

import me.FinestDev.CastleDefense.Main;
import me.FinestDev.CastleDefense.Events.SpectatorInteract;
import me.FinestDev.CastleDefense.Items.Items;
import me.FinestDev.CastleDefense.Kits.KitNebulanaut;
import me.FinestDev.CastleDefense.Kits.KitNebulax;
import me.FinestDev.CastleDefense.Kits.KitWizard;
import me.FinestDev.CastleDefense.Kits.Kits;
import me.FinestDev.CastleDefense.Utilities.BossBarClass;
import me.FinestDev.CastleDefense.Utilities.Util;
import me.FinestDev.CastleDefense.Villagers.SpawnVillagers;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;

public class GameStop {
	
	Main pl;
	public GameStop(Main in){
		pl = in;
	}
	
	public void gameStop(){

		// Stop our timer
		pl.gametimer.cancelTimer();
		
		// Remove our villagers
		pl.spawnvillagers.villagerEffectsStop();
		pl.spawnvillagers.villagerTeleportStop();
		pl.spawnvillagers.killVillagers();
		
		Util.gameInProgress = false;
		
		// Cycle through all the players
		for(Player p : Bukkit.getOnlinePlayers()){
			
			Util.removeTeam(p);
			// Clear inventory & armor
			p.getInventory().clear();
			p.getInventory().setArmorContents(null);
			
			// Teleport them
			p.teleport(Util.lobbySpawn);
			
			for(PotionEffect effect : p.getActivePotionEffects()){
			    p.removePotionEffect(effect.getType());
			}
			
			p.setGameMode(GameMode.SURVIVAL);
			
			// Set their HP to 20
			p.setHealth(20.0);
			p.setAllowFlight(false);
			
			p.getInventory().addItem(Items.menu);
			
			pl.scoreboard.removeScoreBoard(p);
			
			Kits.removeKit(p);
			
		}
		
		// Reset-up our world
		Util.unloadWorld();
		Util.loadWorld();

		// Clear all teams
		Util.redTeam.clear();
		Util.blueTeam.clear();
		SpectatorInteract.specTeam.clear();
		
		KitNebulanaut.locs.clear();
		KitWizard.cooldownShoot.clear();
		KitNebulax.sonicBoomCD.clear();
		
		SpawnVillagers.lastBlue = "";
		SpawnVillagers.lastRed = "";
		
		pl.compassclass.cancelTask();
		pl.scoreboard.stopScoreBoard();
		
		BossBarClass.removeBar();
		
		// Reregister all of our variables
		pl.config.setupConfig();
		
	}

}
