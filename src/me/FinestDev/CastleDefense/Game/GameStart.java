package me.FinestDev.CastleDefense.Game;

import java.util.Arrays;
import java.util.Collections;

import me.FinestDev.CastleDefense.Main;
import me.FinestDev.CastleDefense.Events.SpectatorInteract;
import me.FinestDev.CastleDefense.Items.Items;
import me.FinestDev.CastleDefense.Kits.Kits;
import me.FinestDev.CastleDefense.Utilities.BossBarClass;
import me.FinestDev.CastleDefense.Utilities.Util;
import me.FinestDev.CastleDefense.Villagers.SpawnVillagers;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class GameStart {

	Main pl;
	public GameStart(Main in){
		pl = in;
	}

	public void startGame(){

		pl.scoreboard.createScoreBoard();
		
		Player[] allPlayers = Bukkit.getOnlinePlayers();

		// Here's where we'll kick the normals for the VIPs
		int maxPlayers = Util.maxPlayers;

		int VIPs = 0;
		int regs = 0;

		for(Player p : allPlayers){
			if(!p.hasPermission("fanchallenge.VIP")){
				regs++;
			}else{
				VIPs++;
			}
		}



		// Server is over-filled (so we have to kick some regulars, no VIPs will be kicked)
		if(regs+VIPs > maxPlayers){
			int toKick = 0;
			int x = 0;
			toKick = (regs+VIPs)-maxPlayers;
			if(regs >= toKick){
				// There are enough regulars to kick
				while(toKick > 0){
					if(!allPlayers[x].hasPermission("fanchallenge.VIP")){
						allPlayers[x].kickPlayer(Util.kickedForVIP);
						toKick--;
					}
				}
			}
		}

		// To make it random
		Collections.shuffle(Arrays.asList(allPlayers));


		// This is where players join the teams
		for(Player p : allPlayers){
			if(!Util.redTeam.contains(p.getName()) && !Util.blueTeam.contains(p.getName()) && !SpectatorInteract.specTeam.contains(p.getName())){
				// They're not already on a team
				int redSize = Util.redTeam.size();
				int blueSize = Util.blueTeam.size();

				Location blueSpawn = Util.blueSpawn;
				Location redSpawn = Util.redSpawn;

				p.setGameMode(GameMode.SURVIVAL);
				p.setHealth(20.0);
				p.setFoodLevel(20);
				p.getInventory().clear();
				p.getInventory().setArmorContents(null);
				p.setExp(0);
				
				if(redSize >= blueSize){
					// Join the blue team
					Util.addToBlue(p);
					Util.teleportPlayer(p, blueSpawn);
					Kits.equipKit(p, "Blue");
				}else{
					// Join the red team
					Util.addToRed(p);
					Util.teleportPlayer(p, redSpawn);
					Kits.equipKit(p, "Red");
				}
				
				p.getInventory().addItem(Items.compass);
				
				pl.scoreboard.makeScoreBoard(p);
			}
		}

		// Now we broadcast our message
		Util.broadcastGlobalMessage(Util.gameStartMSG);

		// Start the game!
		Util.gameInProgress = true;
		pl.gametimer.startGameTimer();

		pl.spawnvillagers.spawnVillagers();

		pl.spawnvillagers.villagerEffectsStart();
		pl.spawnvillagers.villagerTeleportStart();
		
		pl.compassclass.compassTimer();

		BossBarClass.updateBars(SpawnVillagers.redHP, SpawnVillagers.blueHP);

	}

}
