package me.FinestDev.CastleDefense.Timers;

import org.bukkit.Bukkit;

import me.FinestDev.CastleDefense.Main;
import me.FinestDev.CastleDefense.Utilities.Util;
import me.FinestDev.CastleDefense.Villagers.SpawnVillagers;

public class GameTimer {

	Main pl;
	public GameTimer(Main in){
		pl = in;
	}

	public static int gameTimer;
	private int Task;

	public void startGameTimer(){
		gameTimer = Util.gameTimer;

		Task = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(pl, new Runnable() {
			public void run() {

				if(gameTimer >= 0 && Util.gameInProgress){
					// The game is still going on

					// Make sure there are people on each team
					if(Util.blueTeam.size() <= 0){
						Util.broadcastGlobalMessage(Util.redGameOver);
						pl.gamestop.gameStop();
					}else

						if(Util.redTeam.size() <= 0){
							Util.broadcastGlobalMessage(Util.blueGameOver);
							pl.gamestop.gameStop();
						}


					SpawnVillagers.lastRedAttack++;
					SpawnVillagers.lastBlueAttack++;

					if(gameTimer%60==0 && gameTimer > 0){
						// Broadcast a minutes message

						Util.broadcastGlobalMessage(Util.gameTimeLeftMin.replace("%time%", (gameTimer/60)+""));

					}else if(gameTimer <= 10 && gameTimer > 0){
						// 10 seconds or less left

						Util.broadcastGlobalMessage(Util.gameTimeLeftSec.replace("%time%", gameTimer+""));

					}

					if(gameTimer == 0){
						// Game over!

						if(SpawnVillagers.redHP > SpawnVillagers.blueHP){
							Util.broadcastGlobalMessage(Util.redMOREHP);
						}else
							if(SpawnVillagers.blueHP > SpawnVillagers.redHP){
								Util.broadcastGlobalMessage(Util.blueMOREHP);
							}else{
								Util.broadcastGlobalMessage(Util.gameDrawMSG);
							}
						pl.gamestop.gameStop();
					}

					gameTimer--;

				}


			}
		},20L,20L);



	}

	public void cancelTimer(){
		Bukkit.getScheduler().cancelTask(Task);
		Util.gameInProgress = false;
	}

}
