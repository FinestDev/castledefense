package me.FinestDev.CastleDefense.Commands;

import me.FinestDev.CastleDefense.Main;
import me.FinestDev.CastleDefense.Utilities.Util;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Commands implements CommandExecutor {

	Main pl;
	public Commands(Main in){
		pl = in;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){

		// Verify that the console isn't sending a command
		if(sender instanceof Player){

			// The player
			Player p = (Player) sender;

			if(cmd.getName().equalsIgnoreCase("startchallenge")){

				if(p.hasPermission("fanchallenge.start")){

					if(!Util.gameInProgress){
						Util.broadcastPlayerMessage(Util.PstartedMSG, p);
						pl.gamestart.startGame();
					}


				}else{
					// They don't have permission for this
					Util.broadcastPlayerMessage(Util.noPermMSG, p);
				}



			}else if(cmd.getName().equalsIgnoreCase("castledefense")){

				if(p.hasPermission("fanchallenge.admin")){

					if(!Util.gameInProgress){

						if(args.length >= 1){

							if(args[0].equalsIgnoreCase("set")){

								if(args.length == 2){
									if(args[1].equalsIgnoreCase("lobbyspawn")){
										pl.getConfig().set("Game.Locations.Lobby.Spawn", Util.locationToString(p.getLocation()));
										Util.broadcastPlayerMessage(Util.setLocationMSG, p);
									}else if(args[1].equalsIgnoreCase("redspawn")){
										pl.getConfig().set("Game.Locations.Red.Spawn", Util.locationToString(p.getLocation()));
										Util.broadcastPlayerMessage(Util.setLocationMSG, p);
									}else if(args[1].equalsIgnoreCase("bluespawn")){
										pl.getConfig().set("Game.Locations.Blue.Spawn", Util.locationToString(p.getLocation()));
										Util.broadcastPlayerMessage(Util.setLocationMSG, p);
									}else if(args[1].equalsIgnoreCase("redvillagerspawn")){
										pl.getConfig().set("Game.Locations.Red.Villager.Spawn", Util.locationToString(p.getLocation()));
										Util.broadcastPlayerMessage(Util.setLocationMSG, p);
									}else if(args[1].equalsIgnoreCase("bluevillagerspawn")){
										pl.getConfig().set("Game.Locations.Blue.Villager.Spawn", Util.locationToString(p.getLocation()));
										Util.broadcastPlayerMessage(Util.setLocationMSG, p);
									}else if(args[1].equalsIgnoreCase("specspawn")){
										pl.getConfig().set("Game.Locations.Spectator.Spawn", Util.locationToString(p.getLocation()));
										Util.broadcastPlayerMessage(Util.setLocationMSG, p);
									}else if(args[1].equalsIgnoreCase("dev")){
										
									}else{
										Util.broadcastPlayerMessage(Util.commandErrorMSG, p);
									}
									pl.saveConfig();
								}else{
									Util.broadcastPlayerMessage(Util.commandErrorMSG, p);
								}


							}else{
								Util.broadcastPlayerMessage(Util.commandErrorMSG, p);
							}

						}else{
							Util.broadcastPlayerMessage(Util.commandErrorMSG, p);
						}

					}

				}

			}


		}
		return false;

	}
}
