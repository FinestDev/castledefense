package me.FinestDev.CastleDefense.Kits;

import java.util.ArrayList;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import me.FinestDev.CastleDefense.Main;

public class Kits {
	
	Main pl;
	public Kits(Main in){
		pl = in;
	}
	
	public static Inventory kitMenu;
	
	// Regular kits
	public static ArrayList<String> Nebulanaut = new ArrayList<String>();
	public static ArrayList<String> Wizard = new ArrayList<String>();
	public static ArrayList<String> Nebulax = new ArrayList<String>();
	
	public static void equipKit(Player p, String team){
		
		if(Nebulanaut.contains(p.getName())){
			EquipKits.equipNebulanaut(p, team);
		}else
		if(Wizard.contains(p.getName())){
			EquipKits.equipWizard(p, team);
		}else
		if(Nebulax.contains(p.getName())){
			EquipKits.equipNebulax(p, team);
		}else{
			setKit(p, "Nebulanaut");
			EquipKits.equipNebulanaut(p, team);
		}
		
		
	}
	
	public static void setKit(Player p, String k){
		removeKit(p);
		
		switch(k) {
		case "Nebulanaut":
			Nebulanaut.add(p.getName());
			break;
		case "Wizard":
			Wizard.add(p.getName());
			break;
		case "Nebulax":
			Nebulax.add(p.getName());
			break;
		}
	}
	
	public static void removeKit(Player p){
		if(Nebulanaut.contains(p.getName())){
			Nebulanaut.remove(p.getName());
		}
		if(Wizard.contains(p.getName())){
			Wizard.remove(p.getName());
		}
	}

}
