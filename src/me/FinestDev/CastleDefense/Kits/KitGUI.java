package me.FinestDev.CastleDefense.Kits;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Material;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.util.Vector;

import me.FinestDev.CastleDefense.Main;
import me.FinestDev.CastleDefense.Items.Items;
import me.FinestDev.CastleDefense.Utilities.Util;

public class KitGUI implements Listener{

	Main pl;
	public KitGUI(Main in){
		pl = in;
	}

	@EventHandler
	public void onOpenMenu(PlayerInteractEvent e){
		if(Util.gameInProgress == false){
			Player p = e.getPlayer();
			if(p.getItemInHand().equals(Items.menu)){

				e.setCancelled(true);
				p.openInventory(Kits.kitMenu);

			}
		}
	}

	@EventHandler
	public void onChooseKit(InventoryClickEvent e){
		if(Util.gameInProgress == false){
			if(e.getInventory().getName().equals(Kits.kitMenu.getName())){
				ItemStack clicked = e.getCurrentItem();

				if(e.getWhoClicked() instanceof Player){
					Player p = (Player)e.getWhoClicked();

					if(clicked != null){
						if(clicked.getItemMeta() != null){
							if(clicked.getItemMeta().getDisplayName() != null){
								e.setCancelled(true);
								if(clicked.getType() == Material.ENDER_PEARL){
									if(p.hasPermission("castledefense.kit.nebulanaut")){
										Kits.setKit(p, "Nebulanaut");
										Util.broadcastPlayerMessage(Util.nebulanautChoose, p);
										p.closeInventory();
									}else{
										p.closeInventory();
										Util.broadcastPlayerMessage(Util.kitNoPerm, p);
									}
								}else
									if(clicked.getType() == Material.BLAZE_ROD){
										if(p.hasPermission("castledefense.kit.wizard")){
											Kits.setKit(p, "Wizard");
											Util.broadcastPlayerMessage(Util.wizardChoose, p);
											p.closeInventory();
										}else{
											p.closeInventory();
											Util.broadcastPlayerMessage(Util.kitNoPerm, p);
										}
									}else
										if(clicked.getType() == Material.NETHER_STAR){
											if(p.hasPermission("castledefense.kit.nebulax")){
												p.closeInventory();

												if(!Kits.Nebulax.contains(p.getName())){
													Firework fw = (Firework) p.getWorld().spawn(p.getEyeLocation(), Firework.class);
													FireworkMeta meta = fw.getFireworkMeta();
													meta.addEffect(FireworkEffect.builder().withColor(Color.RED).build());
													fw.setFireworkMeta(meta);

													//use meta to customize the firework or add parameters to the method
													fw.setVelocity(new Vector(0,2,0));
													//speed is how fast the firework flies
												}
												
												Kits.setKit(p, "Nebulax");
												Util.broadcastPlayerMessage(Util.nebulaxChoose, p);

											}else{
												p.closeInventory();
												Util.broadcastPlayerMessage(Util.kitNoPerm, p);
											}
										}

							}
						}
					}

				}







			}
		}
	}



}
