package me.FinestDev.CastleDefense.Kits;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import me.FinestDev.CastleDefense.Main;
import me.FinestDev.CastleDefense.Items.Items;
import me.FinestDev.CastleDefense.Utilities.ParticleEffect;
import me.FinestDev.CastleDefense.Utilities.Util;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.util.Vector;

public class KitNebulanaut implements Listener{

	Main pl;
	public KitNebulanaut(Main in){
		pl = in;
	}
	
	
	public static HashMap<Location, String> locs = new HashMap<Location, String>();

	// Double Jump
	@EventHandler
	public void onDoubleJump(PlayerToggleFlightEvent e){
		if(!e.getPlayer().isFlying() && e.getPlayer().getGameMode() == GameMode.SURVIVAL) {
			e.getPlayer().setVelocity(e.getPlayer().getVelocity().add(new Vector(0,0.4,0)));
			e.setCancelled(true);
		}
	}

	// Explosive Arrow
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onHit(final ProjectileHitEvent e){
		if(Util.gameInProgress){
			if(e.getEntity().getShooter() instanceof Player){
				final Player shooter = (Player) e.getEntity().getShooter();
				if(e.getEntity() instanceof Arrow){
					Arrow a = (Arrow)e.getEntity();
					if(Kits.Nebulanaut.contains(shooter.getName())){

						locs.put(a.getLocation(), shooter.getName());
						a.getWorld().createExplosion(a.getLocation(), 1.5f);

					}

					Bukkit.getScheduler().scheduleSyncDelayedTask(pl, new Runnable() {
						public void run(){
							if(e.getEntity().isValid()){
								e.getEntity().remove();
								locs.remove(e.getEntity().getLocation());
							}
						}
					},5L);

					Bukkit.getScheduler().scheduleSyncDelayedTask(pl, new Runnable() {
						public void run(){
							if(Kits.Nebulanaut.contains(shooter.getName()) && Util.gameInProgress){
								int arrows = getArrows(shooter);

								if(arrows == 0){
									shooter.getInventory().addItem(new ItemStack(Material.ARROW, 2));
								}else
									if(arrows == 1){
										shooter.getInventory().addItem(new ItemStack(Material.ARROW, 1));
									}
							}
						}
					},240L);
				}
			}
		}
	}

	public static int getArrows(Player player){
		PlayerInventory inventory = player.getInventory();
		ItemStack[] items = inventory.getContents();
		int has = 0;
		for (ItemStack item : items){
			if ((item != null) && (item.getType() == Material.ARROW) && (item.getAmount() > 0)){
				has += item.getAmount();
			}
		}
		return has;
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onEntityExplode(EntityExplodeEvent e){
		if(Util.gameInProgress){
			Iterator<Block> iter = e.blockList().iterator();
			while (iter.hasNext()) {
				Block b = iter.next();
				if (b.getData() != (byte) 11 && b.getData() != (byte) 14) {
					iter.remove();
				}
			}
		}
	}

	private static ArrayList<String> evasiveCD = new ArrayList<String>();

	@EventHandler
	public void onEvasive(PlayerInteractEvent e){
		final Player p = e.getPlayer();

		if(Util.gameInProgress){
			if(Kits.Nebulanaut.contains(p.getName())){

				if(p.getItemInHand().equals(Items.NEevasive)){

					if(!evasiveCD.contains(p.getName())){

						Block b = p.getLocation().getBlock();
						if (b.getType() != Material.AIR || b.getRelative(BlockFace.DOWN).getType() != Material.AIR) {

							ParticleEffect.EXPLODE.display(p.getLocation(), 1f, 1f, 1f, 0.5f, 100);
							p.setVelocity(e.getPlayer().getVelocity().add(new Vector(0,1,0)));

							// Cooldown
							evasiveCD.add(p.getName());
							Util.broadcastPlayerMessage(Util.nebulantCooldownON, p);
							Bukkit.getScheduler().scheduleSyncDelayedTask(pl, new Runnable() {
								public void run(){

									Util.broadcastPlayerMessage(Util.nebulantCooldownOFF, p);
									evasiveCD.remove(p.getName());

								}
							},200L);
						}
					}
				}
			}
		}
	}



}
