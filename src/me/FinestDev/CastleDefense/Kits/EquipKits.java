package me.FinestDev.CastleDefense.Kits;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.FinestDev.CastleDefense.Main;
import me.FinestDev.CastleDefense.Items.Items;

public class EquipKits {
	
	Main pl;
	public EquipKits(Main in){
		pl = in;
	}
	
	public static void setHelmet(Player p, ItemStack i){
		p.getInventory().setHelmet(i);
	}
	
	public static void setChestplate(Player p, ItemStack i){
		p.getInventory().setChestplate(i);
	}
	
	public static void setLeggings(Player p, ItemStack i){
		p.getInventory().setLeggings(i);
	}
	
	public static void setBoots(Player p, ItemStack i){
		p.getInventory().setBoots(i);
	}
	
	public static void addItem(Player p, ItemStack i){
		p.getInventory().addItem(i);
	}
	
	public static ItemStack fullprotect(ItemStack i){
		i.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		i.addUnsafeEnchantment(Enchantment.PROTECTION_FIRE, 1);
		i.addUnsafeEnchantment(Enchantment.PROTECTION_EXPLOSIONS, 1);
		i.addUnsafeEnchantment(Enchantment.PROTECTION_PROJECTILE, 1);
		i.addUnsafeEnchantment(Enchantment.PROTECTION_FALL, 1);
		
		return i;
	}
	
	public static void equipNebulanaut(Player p, String s){
		p.getInventory().addItem(Items.NEstonesword);
		p.getInventory().addItem(Items.NEevasive);
		p.getInventory().addItem(new ItemStack(Material.BOW, 1));
		p.getInventory().addItem(new ItemStack(Material.ARROW, 2));
		switch(s) {
		case "Red":
			setHelmet(p, fullprotect(Items.rglass));
			setChestplate(p, fullprotect(Items.rchest));
			setLeggings(p, fullprotect(Items.rlegg));
			setBoots(p, fullprotect(Items.rboots));
			p.getInventory().addItem(Items.shears);
			addItem(p, new ItemStack(Material.WOOL, 192, (byte) 14));
			break;
		case "Blue":
			setHelmet(p, fullprotect(Items.bglass));
			setChestplate(p, fullprotect(Items.bchest));
			setLeggings(p, fullprotect(Items.blegg));
			setBoots(p, fullprotect(Items.bboots));
			p.getInventory().addItem(Items.shears);
			addItem(p, new ItemStack(Material.WOOL, 192, (byte) 11));
			break;
		}
		p.setAllowFlight(true);
	}
	
	public static void equipWizard(Player p, String s){
		addItem(p, Items.wizardStaff);
		addItem(p, Items.wizardWand);
		addItem(p, new ItemStack(Material.ENDER_PEARL,1 ));
		switch(s) {
		case "Red":
			setHelmet(p, Items.rhelm);
			p.getInventory().addItem(Items.shears);
			addItem(p, new ItemStack(Material.WOOL, 192, (byte) 14));
			break;
		case "Blue":
			setHelmet(p, Items.bhelm);
			p.getInventory().addItem(Items.shears);
			addItem(p, new ItemStack(Material.WOOL, 192, (byte) 11));
			break;
		}
		
		setChestplate(p, Items.chainChest);
		setBoots(p, Items.chainBoots);
	}
	
	public static void equipNebulax(Player p, String s){
		addItem(p, Items.nebulaxBlade);
		setChestplate(p, fullprotect(Items.dchest));
		addItem(p, Items.nebulaxBomb);
		addItem(p, Items.nebulaxBomb);
		addItem(p, Items.sonicBoom);
		switch(s) {
		case "Red":
			setHelmet(p, Items.rhelm);
			p.getInventory().addItem(Items.shears);
			addItem(p, new ItemStack(Material.WOOL, 192, (byte) 14));
			break;
		case "Blue":
			setHelmet(p, Items.bhelm);
			p.getInventory().addItem(Items.shears);
			addItem(p, new ItemStack(Material.WOOL, 192, (byte) 11));
		}
	}
	

}
