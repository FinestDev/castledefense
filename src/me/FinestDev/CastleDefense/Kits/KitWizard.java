package me.FinestDev.CastleDefense.Kits;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.util.BlockIterator;

import me.FinestDev.CastleDefense.Main;
import me.FinestDev.CastleDefense.Items.Items;
import me.FinestDev.CastleDefense.Utilities.ParticleEffect;
import me.FinestDev.CastleDefense.Utilities.Util;

public class KitWizard implements Listener{

	Main pl;
	public KitWizard(Main in){
		pl = in;
	}

	public static ArrayList<String> cooldownShoot = new ArrayList<String>();

	@EventHandler
	public void onEnderPearl(final PlayerInteractEvent e){
		if(Util.gameInProgress){
			if(Kits.Wizard.contains(e.getPlayer().getName())){
				if(e.getPlayer().getItemInHand().getType() == Material.ENDER_PEARL){
					Bukkit.getScheduler().scheduleSyncDelayedTask(pl, new Runnable() {
						public void run(){
							if(Kits.Wizard.contains(e.getPlayer().getName()) && Util.gameInProgress){
								if(getPearl(e.getPlayer()) <= 0){
									e.getPlayer().getInventory().addItem(new ItemStack(Material.ENDER_PEARL, 1));
								}
							}
						}
					},200L);
				}
			}
		}
	}

	public static int getPearl(Player player){
		PlayerInventory inventory = player.getInventory();
		ItemStack[] items = inventory.getContents();
		int has = 0;
		for (ItemStack item : items){
			if ((item != null) && (item.getType() == Material.ENDER_PEARL) && (item.getAmount() > 0)){
				has += item.getAmount();
			}
		}
		return has;
	}

	@EventHandler
	public void onWand(final PlayerInteractEvent e){
		if(Util.gameInProgress){
			if(Kits.Wizard.contains(e.getPlayer().getName())){
				if(e.getPlayer().getItemInHand().equals(Items.wizardWand)){
					if(!cooldownShoot.contains(e.getPlayer().getName())){
						cooldownShoot.add(e.getPlayer().getName());
						shootWand(e.getPlayer());
						Util.broadcastPlayerMessage(pl.getConfig().getString("Kits.Wizard.Shoot.CooldownON"), e.getPlayer());
						Bukkit.getScheduler().scheduleSyncDelayedTask(pl, new Runnable() {
							public void run(){
								if(Util.gameInProgress && Kits.Wizard.contains(e.getPlayer().getName())){
									Util.broadcastPlayerMessage(pl.getConfig().getString("Kits.Wizard.Shoot.CooldownOFF"), e.getPlayer());
									cooldownShoot.remove(e.getPlayer().getName());
								}
							}
						},80L);
					}
				}
			}
		}
	}

	public void shootWand(final Player player) {

		player.getWorld().playSound(player.getLocation(), Sound.FIREWORK_LAUNCH, 1f, 1f);
		Location loc = player.getEyeLocation();
		BlockIterator bi = new BlockIterator(loc, 0, 10);//range is 100 in this case.
		Location blocktoadd;
		while (bi.hasNext()) {
			blocktoadd = bi.next().getLocation();
			if (blocktoadd.getBlock().getType() != Material.AIR) {
				break;
			}
			ParticleEffect.FIREWORKS_SPARK.display(blocktoadd, 0.02f, .02f, .02f, .02f, 10);
			if(!bi.hasNext()){
				blocktoadd.getWorld().createExplosion(blocktoadd, 1.75f);
				KitNebulanaut.locs.put(blocktoadd, player.getName());
				final Location toExplode = blocktoadd;
				Bukkit.getScheduler().scheduleSyncDelayedTask(pl, new Runnable() {
					public void run(){
						KitNebulanaut.locs.remove(toExplode);
					}
				},20L);
			}
		}
	}

}