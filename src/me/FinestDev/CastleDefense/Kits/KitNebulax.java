package me.FinestDev.CastleDefense.Kits;

import java.util.ArrayList;

import me.FinestDev.CastleDefense.Main;
import me.FinestDev.CastleDefense.Items.Items;
import me.FinestDev.CastleDefense.Utilities.Util;
import me.FinestDev.CastleDefense.Villagers.KillVillager;
import me.FinestDev.CastleDefense.Villagers.SpawnVillagers;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class KitNebulax implements Listener{

	Main pl;
	public KitNebulax(Main in){
		pl = in;
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onBomb(ProjectileHitEvent e){
		if(Util.gameInProgress){
			if(e.getEntity() instanceof Snowball){
				final Snowball snowball = (Snowball) e.getEntity();

				if(snowball.getShooter() instanceof Player){
					final Player p = (Player) snowball.getShooter();
					if(Kits.Nebulax.contains(p.getName())){

						KitNebulanaut.locs.put(snowball.getLocation(), p.getName());
						snowball.getWorld().createExplosion(snowball.getLocation(), 2f);

						Bukkit.getScheduler().scheduleSyncDelayedTask(pl, new Runnable() {
							public void run(){
								KitNebulanaut.locs.remove(snowball.getLocation());	
							}
						},10L);
						
						Bukkit.getScheduler().scheduleSyncDelayedTask(pl, new Runnable() {
							public void run(){
								if(Util.gameInProgress && Kits.Nebulax.contains(p.getName())){
									if(getBombs(p) == 0){
										p.getInventory().addItem(Items.nebulaxBomb);
										p.getInventory().addItem(Items.nebulaxBomb);
									}else
									if(getBombs(p) == 1){
										p.getInventory().addItem(Items.nebulaxBomb);
									}
								}
							}
						},240L);


					}
				}

			}
		}
	}

	public static int getBombs(Player player){
		PlayerInventory inventory = player.getInventory();
		ItemStack[] items = inventory.getContents();
		int has = 0;
		for (ItemStack item : items){
			if ((item != null) && (item.getType() == Material.SNOW_BALL) && (item.getAmount() > 0)){
				has += item.getAmount();
			}
		}
		return has;
	}
	

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onSnowballHIT(EntityDamageByEntityEvent e){
		if(Util.gameInProgress){
			if(e.getEntity() instanceof Villager){
				if(e.getDamager() instanceof Snowball){
					Snowball ball = (Snowball) e.getDamager();
					if(ball.getShooter() instanceof Player){
						Player p = (Player)ball.getShooter();

						if(e.getEntity() == SpawnVillagers.blueVillager){
							if(Util.blueTeam.contains(p.getName())){
								e.setDamage(0.0);
								e.setCancelled(true);
							}else{
								KillVillager.updateVillager("Blue", e.getDamage());
							}
						}else
							if(e.getEntity() == SpawnVillagers.redVillager){
								if(Util.redTeam.contains(p.getName())){
									e.setDamage(0.0);
									e.setCancelled(true);
								}else{
									KillVillager.updateVillager("Red", e.getDamage());
								}
							}

					}

				}

			}
		}
	}

	public static ArrayList<String> sonicBoomCD = new ArrayList<String>();
	@EventHandler
	public void onSonicBoom(PlayerInteractEvent e){
		if(Util.gameInProgress){
			final Player p = e.getPlayer();
			if(Kits.Nebulax.contains(p.getName())){
				if(p.getItemInHand().equals(Items.sonicBoom)){
					if(!sonicBoomCD.contains(p.getName())){
						sonicBoomCD.add(p.getName());
						p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 200, 2));
						Util.broadcastPlayerMessage(pl.getConfig().getString("Kits.Nebulax.Speed.CooldownON"), p);

						Bukkit.getScheduler().scheduleSyncDelayedTask(pl, new Runnable() {
							public void run(){
								if(Util.gameInProgress && Kits.Nebulax.contains(p.getName())){
									Util.broadcastPlayerMessage(pl.getConfig().getString("Kits.Nebulax.Speed.CooldownOFF"), p);
									sonicBoomCD.remove(p.getName());
								}
							}
						},300L);
					}
				}
			}
		}
	}

}
