package me.FinestDev.CastleDefense.Utilities;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import me.FinestDev.CastleDefense.Main;
import me.FinestDev.CastleDefense.Events.SpectatorInteract;
import me.FinestDev.CastleDefense.Villagers.SpawnVillagers;

public class ScoreboardClass {

	Main pl;
	public ScoreboardClass(Main in){
		pl = in;
	}

	public Scoreboard board;

	Objective objective;
	ScoreboardManager manager;


	Score blueVILL;
	Score redVILL;
	Score blueCount;
	Score redCount;

	Score name2;
	Score villHealth;
	Score playerCounts;

	Score specCount;
	private int Task;
	
	public void createScoreBoard(){

		manager = Bukkit.getScoreboardManager();
		board = manager.getNewScoreboard();

		objective = board.registerNewObjective("castledefense", "dummy");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);

		objective.setDisplayName(ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("ScoreBoard.Name")));

		name2 = objective.getScore(ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("ScoreBoard.FanChallenge")));
		name2.setScore(14);

		// Blank
		objective.getScore("").setScore(13);

		villHealth = objective.getScore(ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("ScoreBoard.VillagerHealth")));
		villHealth.setScore(12);

		blueVILL = objective.getScore(ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("ScoreBoard.Blue.Villager").replace("%hp%", SpawnVillagers.blueHP+"")));
		blueVILL.setScore(11);
		redVILL = objective.getScore(ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("ScoreBoard.Red.Villager").replace("%hp%", SpawnVillagers.redHP+"")));
		redVILL.setScore(10);

		// Blank
		objective.getScore(" ").setScore(9);

		playerCounts = objective.getScore(ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("ScoreBoard.PlayerCount")));
		playerCounts.setScore(8);

		blueCount = objective.getScore(ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("ScoreBoard.Blue.Players").replace("%amount%", Util.blueTeam.size()+"")));
		blueCount.setScore(7);

		redCount = objective.getScore(ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("ScoreBoard.Red.Players").replace("%amount%", Util.redTeam.size()+"")));
		redCount.setScore(6);

		specCount = objective.getScore(ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("ScoreBoard.Spectator.Players").replace("%amount%", SpectatorInteract.specTeam.size()+"")));
		specCount.setScore(5);

		// Blank
		objective.getScore("  ").setScore(4);

		// IP
		objective.getScore(ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("ScoreBoard.IP"))).setScore(3);

		// Site
		objective.getScore(ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("ScoreBoard.Site"))).setScore(2);

		// End
		objective.getScore(ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("ScoreBoard.Name"))).setScore(1);


		Task = Bukkit.getScheduler().scheduleSyncRepeatingTask(pl, new Runnable() {

			public void run(){

				manager = Bukkit.getScoreboardManager();
				board = manager.getNewScoreboard();

				objective = board.registerNewObjective("castledefense", "dummy");
				objective.setDisplaySlot(DisplaySlot.SIDEBAR);

				objective.setDisplayName(ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("ScoreBoard.Name")));

				name2 = objective.getScore(ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("ScoreBoard.FanChallenge")));
				name2.setScore(14);

				// Blank
				objective.getScore("").setScore(13);

				villHealth = objective.getScore(ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("ScoreBoard.VillagerHealth")));
				villHealth.setScore(12);

				blueVILL = objective.getScore(ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("ScoreBoard.Blue.Villager").replace("%hp%", getBlueHP())));
				blueVILL.setScore(11);
				redVILL = objective.getScore(ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("ScoreBoard.Red.Villager").replace("%hp%", getRedHP())));
				redVILL.setScore(10);

				// Blank
				objective.getScore(" ").setScore(9);

				playerCounts = objective.getScore(ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("ScoreBoard.PlayerCount")));
				playerCounts.setScore(8);

				blueCount = objective.getScore(ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("ScoreBoard.Blue.Players").replace("%amount%", Util.blueTeam.size()+"")));
				blueCount.setScore(7);

				redCount = objective.getScore(ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("ScoreBoard.Red.Players").replace("%amount%", Util.redTeam.size()+"")));
				redCount.setScore(6);

				specCount = objective.getScore(ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("ScoreBoard.Spectator.Players").replace("%amount%", SpectatorInteract.specTeam.size()+"")));
				specCount.setScore(5);

				// Blank
				objective.getScore("  ").setScore(4);

				// IP
				objective.getScore(ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("ScoreBoard.IP"))).setScore(3);

				// Site
				objective.getScore(ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("ScoreBoard.Site"))).setScore(2);

				// End
				objective.getScore(ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("ScoreBoard.Name"))).setScore(1);

				for(Player p : Bukkit.getOnlinePlayers()){
					if(Util.redTeam.contains(p.getName()) || Util.blueTeam.contains(p.getName()) ||
							SpectatorInteract.specTeam.contains(p.getName())){
						p.setScoreboard(board);
					}
				}


			}

		},40L,40L);


	}
	
	public String getRedHP(){
		if(SpawnVillagers.redHP == 1500){
			return "▓▓▓▓▓▓▓";
		}
		if(SpawnVillagers.redHP > 1300){
			return "▓▓▓▓▓▓░";
		}else
		if(SpawnVillagers.redHP > 1100){
			return "▓▓▓▓▓░░";
		}else
		if(SpawnVillagers.redHP > 1000){
			return "▓▓▓▓▓░░";
			}else
		if(SpawnVillagers.redHP > 800){
			return "▓▓▓▓▓░░";
			}else
		if(SpawnVillagers.redHP > 600){
			return "▓▓▓▓░░░";
		}else
		if(SpawnVillagers.redHP > 400){
			return "▓▓▓░░░░";
		}else
		if(SpawnVillagers.redHP > 200){
			return "▓░░░░░░";
		}else
		if(SpawnVillagers.redHP > 100){
			return "▓░░░░░░";
		}else
		if(SpawnVillagers.redHP > 80){
			return "▓░░░░░░";
		}else
		if(SpawnVillagers.redHP > 60){
			return "▓░░░░░░";
		}else
		if(SpawnVillagers.redHP > 40){
			return "▓░░░░░░";
		}else
		if(SpawnVillagers.redHP > 20){
			return "▓░░░░░░";
		}else
			return "░░░░░░░";
	}
	
	public String getBlueHP(){
		if(SpawnVillagers.blueHP == 1500){
			return "▓▓▓▓▓▓▓";
		}
		if(SpawnVillagers.blueHP > 1300){
			return "▓▓▓▓▓░░";
		}else
		if(SpawnVillagers.blueHP > 1100){
			return "▓▓▓▓▓░░";
		}else
		if(SpawnVillagers.blueHP > 1000){
			return "▓▓▓▓▓░░";
			}else
		if(SpawnVillagers.blueHP > 800){
			return "▓▓▓▓▓░░";
			}else
		if(SpawnVillagers.blueHP > 600){
			return "▓▓▓▓░░░";
		}else
		if(SpawnVillagers.blueHP > 400){
			return "▓▓▓░░░░";
		}else
		if(SpawnVillagers.blueHP > 200){
			return "▓░░░░░░";
		}else
		if(SpawnVillagers.blueHP > 100){
			return "▓░░░░░░";
		}else
		if(SpawnVillagers.blueHP > 80){
			return "▓░░░░░░";
		}else
		if(SpawnVillagers.blueHP > 60){
			return "▓░░░░░░";
		}else
		if(SpawnVillagers.blueHP > 40){
			return "▓░░░░░░";
		}else
		if(SpawnVillagers.blueHP > 20){
			return "▓░░░░░░";
		}else
			return "░░░░░░░";
	}

	public void stopScoreBoard(){
		Bukkit.getScheduler().cancelTask(Task);
	}

	public void makeScoreBoard(Player p){
		p.setScoreboard(board);
	}

	public void removeScoreBoard(Player p){
		Scoreboard blank = manager.getNewScoreboard();
		p.setScoreboard(blank);
	}

}
