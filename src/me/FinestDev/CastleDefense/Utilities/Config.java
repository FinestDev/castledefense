package me.FinestDev.CastleDefense.Utilities;

import org.bukkit.ChatColor;

import me.FinestDev.CastleDefense.Main;
import me.FinestDev.CastleDefense.Villagers.SpawnVillagers;

public class Config {
	
	Main pl;
	public Config(Main in){
		pl = in;
	}
	
	// Set all of our variables
	public void setupConfig(){
		
		// Kits
		Util.kitNoPerm = pl.getConfig().getString("Kits.NoPerm");
		Util.nebulanautChoose = pl.getConfig().getString("Kits.Nebulanaut.Choose");
		Util.wizardChoose = pl.getConfig().getString("Kits.Wizard.Choose");
		Util.nebulaxChoose = pl.getConfig().getString("Kits.Nebulax.Choose");
		
		Util.nebulantCooldownON = pl.getConfig().getString("Kits.Nebulanaut.Evasion.CooldownON");
		Util.nebulantCooldownOFF = pl.getConfig().getString("Kits.Nebulanaut.Evasion.CooldownOFF");
		
		Util.menuName = ChatColor.translateAlternateColorCodes('$', pl.getConfig().getString("Kits.Name"));
		
		// Misc.
		Util.maxPlayers = pl.getConfig().getInt("Game.MaxPlayers");
		
		// Timers
		Util.gameTimer = pl.getConfig().getInt("Game.Timers.Game");
		
		// Chat
		Util.chatPrefix = pl.getConfig().getString("Chat.Prefix");
		Util.noPermMSG = pl.getConfig().getString("Chat.Error.NoPermission");
		Util.AstartedMSG = pl.getConfig().getString("Chat.Error.AlreadyStarted");
		Util.PstartedMSG = pl.getConfig().getString("Chat.PlayerStartMSG");
		
		Util.kickedForVIP = pl.getConfig().getString("Chat.Error.MakeRoom");
		
		Util.joinBlueMSG = pl.getConfig().getString("Chat.Join.BlueMSG");
		Util.joinRedMSG = pl.getConfig().getString("Chat.Join.RedMSG");
		Util.joinSpecMSG = pl.getConfig().getString("Chat.Join.SpecMSG");
		Util.attackTeamMSG = pl.getConfig().getString("Chat.Attack.SameTeamMSG");
		Util.deathMSG = pl.getConfig().getString("Chat.Attack.DeathMSG");
		
		Util.villagerBlueDeathMSG = pl.getConfig().getString("Chat.Game.Villager.Blue.Death");
		Util.villagerRedDeathMSG = pl.getConfig().getString("Chat.Game.Villager.Red.Death");
		Util.villagerUnknownDeath = pl.getConfig().getString("Chat.Game.Villager.MiscDeath");
		Util.gameDrawMSG = pl.getConfig().getString("Chat.Game.DrawMSG");
		Util.blueFFMSG = pl.getConfig().getString("Chat.Game.Blue.FF");
		Util.redFFMSG = pl.getConfig().getString("Chat.Game.Red.FF");
		
		Util.redGameOver = pl.getConfig().getString("Chat.Game.GameOver.Red");
		Util.blueGameOver = pl.getConfig().getString("Chat.Game.GameOver.Blue");
		
		Util.villagerBlueBarMSG = pl.getConfig().getString("Chat.Game.Villager.Blue.BarMessage");
		Util.villagerRedBarMSG = pl.getConfig().getString("Chat.Game.Villager.Red.BarMessage");
		
		Util.commandErrorMSG = pl.getConfig().getString("Chat.Error.Invalid");
		
		Util.joinInProgMSG = pl.getConfig().getString("Chat.Join.KickMSG");
		Util.gameTimeLeftMin = pl.getConfig().getString("Chat.Game.MinMSG");
		Util.gameTimeLeftSec = pl.getConfig().getString("Chat.Game.SecMSG");
				
		Util.gameStartMSG = pl.getConfig().getString("Chat.Game.StartMSG");
		
		Util.compassMSG = pl.getConfig().getString("Chat.Game.CompassMSG");
		
		Util.blueMOREHP = pl.getConfig().getString("Chat.Game.Blue.MoreHP");
		Util.redMOREHP = pl.getConfig().getString("Chat.Game.Red.MoreHP");
		
		Util.redUnderAttack = pl.getConfig().getString("Chat.Game.Red.UnderAttack");
		Util.blueUnderAttack = pl.getConfig().getString("Chat.Game.Blue.UnderAttack");
		
		// Villager HP's
		SpawnVillagers.redHP = pl.getConfig().getDouble("Game.Locations.Red.Villager.Health");
		SpawnVillagers.blueHP = pl.getConfig().getDouble("Game.Locations.Blue.Villager.Health");
		
		// Locations
		Util.redSpawn = Util.stringToLocation(pl.getConfig().getString("Game.Locations.Red.Spawn"));
		Util.blueSpawn = Util.stringToLocation(pl.getConfig().getString("Game.Locations.Blue.Spawn"));
		Util.lobbySpawn = Util.stringToLocation(pl.getConfig().getString("Game.Locations.Lobby.Spawn"));
		Util.specSpawn = Util.stringToLocation(pl.getConfig().getString("Game.Locations.Spectator.Spawn"));
		
		Util.setLocationMSG = pl.getConfig().getString("Chat.SetLocationMSG");
		
		Util.redVillager = Util.stringToLocation(pl.getConfig().getString("Game.Locations.Red.Villager.Spawn"));
		Util.blueVillager = Util.stringToLocation(pl.getConfig().getString("Game.Locations.Blue.Villager.Spawn"));
		
		
	}
	
	

}
