package me.FinestDev.CastleDefense.Utilities;

import me.FinestDev.CastleDefense.Villagers.SpawnVillagers;
import me.mgone.bossbarapi.BossbarAPI;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class BossBarClass {

	// Red, Blue
	public static void updateBars(double redHP, double blueHP){

		for(Player p : Bukkit.getOnlinePlayers()){
			if(Util.redTeam.contains(p.getName())){

				if(!SpawnVillagers.bv.isDead()){
					BossbarAPI.setMessage(p, ChatColor.translateAlternateColorCodes('$', Util.villagerBlueBarMSG.replace("%hp%", blueHP+"")));
				}else{
					BossbarAPI.setMessage(p, ChatColor.translateAlternateColorCodes('$', Util.villagerBlueBarMSG.replace("%hp%", "0")));
				}

			}else
				if(Util.blueTeam.contains(p.getName())){

					if(!SpawnVillagers.rv.isDead()){
						BossbarAPI.setMessage(p, ChatColor.translateAlternateColorCodes('$', Util.villagerRedBarMSG.replace("%hp%", redHP+"")));
					}else{
						BossbarAPI.setMessage(p, ChatColor.translateAlternateColorCodes('$', Util.villagerRedBarMSG.replace("%hp%", "0")));
					}
				}
		}
	}

	@SuppressWarnings("deprecation")
	public static void removeBar(){
		for(Player p : Bukkit.getOnlinePlayers()){

			if(BossbarAPI.hasBar(p)){
				BossbarAPI.removeBar(p);
			}

		}
	}

}
