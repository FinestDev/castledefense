package me.FinestDev.CastleDefense.Utilities;

import java.lang.reflect.Field;
import java.util.ArrayList;

import me.FinestDev.CastleDefense.Events.SpectatorInteract;
import net.minecraft.server.v1_7_R4.EntityPlayer;
import net.minecraft.server.v1_7_R4.PacketPlayOutNamedEntitySpawn;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;

public class Util {

	// Teams
	public static ArrayList<String> blueTeam = new ArrayList<String>();
	public static ArrayList<String> redTeam = new ArrayList<String>();

	// Kits
	public static String kitNoPerm;
	public static String nebulanautChoose;
	public static String wizardChoose;
	public static String nebulaxChoose;

	public static String menuName;

	public static String nebulantCooldownON;
	public static String nebulantCooldownOFF;

	// Timers
	public static int gameTimer;
	public static String gameWorld;

	// Timers Chat
	public static String gameTimeLeftMin;
	public static String gameTimeLeftSec;

	// Chat
	public static String chatPrefix;
	public static String noPermMSG;
	public static String AstartedMSG;
	public static String PstartedMSG;
	public static String joinInProgMSG;

	public static String kickedForVIP;

	public static String gameStartMSG;

	public static String deathMSG;
	public static String gameEndMSG;
	public static String gameDrawMSG;

	public static String villagerBlueBarMSG;
	public static String villagerRedBarMSG;

	public static String blueMOREHP;
	public static String redMOREHP;
	public static String redGameOver;
	public static String blueGameOver;

	public static String villagerUnknownDeath;

	public static String commandErrorMSG;

	public static int maxPlayers;

	// Items MISC
	public static String compassMSG;

	// Teams Chat
	public static String joinRedMSG;
	public static String joinBlueMSG;
	public static String joinSpecMSG;

	public static String attackTeamMSG;
	public static String villagerRedDeathMSG;
	public static String villagerBlueDeathMSG;
	
	public static String blueUnderAttack;
	public static String redUnderAttack;

	// Locations
	public static Location redSpawn;
	public static Location blueSpawn;
	public static Location specSpawn;
	public static Location lobbySpawn;

	public static String blueFFMSG;
	public static String redFFMSG;

	public static String setLocationMSG;

	public static Location redVillager;
	public static Location blueVillager;

	// Booleans
	public static boolean gameInProgress = false;

	// Chat Utilities
	public static void broadcastGlobalMessage(String msg){

		Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('$', chatPrefix+" "+msg));

	}

	public static void broadcastPlayerMessage(String msg, Player p){

		p.sendMessage(ChatColor.translateAlternateColorCodes('$', chatPrefix+" "+msg));

	}

	// Teams Utilities
	public static void addToRed(Player p){
		p.setDisplayName("�c"+p.getDisplayName());
		setNameTag(p, "�c");
		redTeam.add(p.getName());
		broadcastPlayerMessage(joinRedMSG, p);
	}

	public static void addToBlue(Player p){
		p.setDisplayName("�b"+p.getDisplayName());
		setNameTag(p, "�b");
		blueTeam.add(p.getName());
		broadcastPlayerMessage(joinBlueMSG, p);
	}

	public static void addToSpec(Player p){
		SpectatorInteract.specTeam.add(p.getName());
		broadcastPlayerMessage(joinSpecMSG, p);
	}

	public static void removeTeam(Player p){
		if(redTeam.contains(p.getName())){
			redTeam.remove(p.getName());
		}
		if(blueTeam.contains(p.getName())){
			blueTeam.remove(p.getName());
		}
		if(SpectatorInteract.specTeam.contains(p.getName())){
			SpectatorInteract.specTeam.remove(p.getName());
		}
		clearNameTag(p);
	}
	
	public static void setNameTag(Player p, String c){
		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "ne prefix "+p.getName()+" "+c);
	}
	
	public static void clearNameTag(Player p){
		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "ne clear "+p.getName());
		p.setDisplayName(ChatColor.stripColor(p.getDisplayName()));
	}

	// World Utilities
	public static void loadWorld(){
		Bukkit.getServer().createWorld(new WorldCreator(Util.gameWorld));
		World arenamap = Bukkit.getWorld(Util.gameWorld);
		arenamap.setAutoSave(false);

		for(Entity i : Bukkit.getWorld(Util.gameWorld).getEntities()){
			if(i instanceof Item || i instanceof Arrow || i instanceof Villager){
				i.remove();
			}
		}
	}

	public static void unloadWorld(){
		Bukkit.getServer().unloadWorld(Util.gameWorld, true);
	}

	// Locations Utilities

	public static void teleportPlayer(Player p, Location l){
		p.teleport(l);
	}

	public static Location stringToLocation(String s){
		String[] sp = s.split(",");
		// Return the location from a string [World, X, Y, Z]
		return new Location(Bukkit.getWorld(sp[0]), Integer.parseInt(sp[1]), Integer.parseInt(sp[2]), Integer.parseInt(sp[3]));
	}

	public static String locationToString(Location l){
		return l.getWorld().getName()+","+l.getBlockX()+","+l.getBlockY()+","+l.getBlockZ();
	}



}
