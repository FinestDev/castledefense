package me.FinestDev.CastleDefense.Villagers;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Villager.Profession;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.FinestDev.CastleDefense.Main;
import me.FinestDev.CastleDefense.Utilities.ParticleEffect;
import me.FinestDev.CastleDefense.Utilities.Util;

public class SpawnVillagers {

	Main pl;
	public SpawnVillagers(Main in){
		pl = in;
	}

	public static Entity redVillager;
	public static Entity blueVillager;

	public static Villager rv;
	public static Villager bv;

	public static double redHP;
	public static double blueHP;

	private int EffectsID;
	private int VillID;
	
	public static String lastBlue;
	public static String lastRed;
	
	public static int lastRedAttack;
	public static int lastBlueAttack;

	public void spawnVillagers(){

		Location red = Util.redVillager;
		Location blue = Util.blueVillager;

		redVillager = red.getWorld().spawnEntity(red, EntityType.VILLAGER);
		blueVillager = blue.getWorld().spawnEntity(blue, EntityType.VILLAGER);

		// Prevent movement
		rv = (Villager) redVillager;
		rv.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 300));

		bv = (Villager) blueVillager;
		bv.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 300));

		rv.setMaxHealth(redHP);
		bv.setMaxHealth(blueHP);
		rv.setHealth(redHP);
		bv.setHealth(blueHP);
		
		rv.setProfession(Profession.LIBRARIAN);
		bv.setProfession(Profession.LIBRARIAN);

	}

	public void villagerEffectsStart(){
		EffectsID = Bukkit.getScheduler().scheduleSyncRepeatingTask(pl, new Runnable() {
			public void run(){
				if(!SpawnVillagers.rv.isDead() && !SpawnVillagers.bv.isDead()){
					runHelixRed(rv.getLocation());
					runHelixBlue(bv.getLocation());	
				}
			}
		},9L,9L);
	}

	public void villagerTeleportStart(){
		VillID = Bukkit.getScheduler().scheduleSyncRepeatingTask(pl, new Runnable() {
			public void run(){
				if(rv.getLocation().distance(Util.redVillager) > .01){
					rv.teleport(Util.redVillager);
				}

				if(bv.getLocation().distance(Util.blueVillager) > .01){
					bv.teleport(Util.blueVillager);
				}
			}
		},2L,2L);
	}

	public void villagerTeleportStop(){
		Bukkit.getScheduler().cancelTask(VillID);
	}

	public void villagerEffectsStop(){
		Bukkit.getScheduler().cancelTask(EffectsID);
	}

	public void killVillagers(){

		if(!rv.isValid()){
			rv.remove();
		}
		if(!bv.isValid()){
			bv.remove();
		}

	}

	// Effects
	public static void runHelixRed(Location loc) {

		double radius = 2;

		for (double y = 5; y >= 0; y -= 0.007) {
			radius = y / 3;
			double x = radius * Math.cos(3 * y);
			double z = radius * Math.sin(3 * y);

			double y2 = 5 - y;

			Location loc2 = new Location(loc.getWorld(), loc.getX() + x, loc.getY() + y2, loc.getZ() + z);
			ParticleEffect.RED_DUST.display(loc2, 0, 0, 0, 0, 1);
		}

		for (double y = 5; y >= 0; y -= 0.007) {
			radius = y / 3;
			double x = -(radius * Math.cos(3 * y));
			double z = -(radius * Math.sin(3 * y));

			double y2 = 5 - y;

			Location loc2 = new Location(loc.getWorld(), loc.getX() + x, loc.getY() + y2, loc.getZ() + z);
			ParticleEffect.RED_DUST.display(loc2, 0, 0, 0, 0, 1);
		}

	}
	
	public static void runHelixBlue(Location loc) {

		double radius = 2;

		for (double y = 5; y >= 0; y -= 0.007) {
			radius = y / 3;
			double x = radius * Math.cos(3 * y);
			double z = radius * Math.sin(3 * y);

			double y2 = 5 - y;

			Location loc2 = new Location(loc.getWorld(), loc.getX() + x, loc.getY() + y2, loc.getZ() + z);
			ParticleEffect.SPLASH.display(loc2, 0, 0, 0, 0, 1);
		}

		for (double y = 5; y >= 0; y -= 0.007) {
			radius = y / 3;
			double x = -(radius * Math.cos(3 * y));
			double z = -(radius * Math.sin(3 * y));

			double y2 = 5 - y;

			Location loc2 = new Location(loc.getWorld(), loc.getX() + x, loc.getY() + y2, loc.getZ() + z);
			ParticleEffect.SPLASH.display(loc2, 0, 0, 0, 0, 1);
		}

	}

}
