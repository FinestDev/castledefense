package me.FinestDev.CastleDefense.Villagers;

import java.util.HashMap;
import java.util.Map;

import me.FinestDev.CastleDefense.Main;
import me.FinestDev.CastleDefense.Kits.KitNebulanaut;
import me.FinestDev.CastleDefense.Utilities.BossBarClass;
import me.FinestDev.CastleDefense.Utilities.Util;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEntityEvent;

public class KillVillager implements Listener{

	Main pl;
	public KillVillager(Main in){
		pl = in;
	}

	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.HIGH)// This is JUST to cancel player DMG to the villager
	public void onDMG(EntityDamageByEntityEvent e){

		if(Util.gameInProgress){

			// If it's our Villager
			if(e.getEntity() == SpawnVillagers.blueVillager || e.getEntity() == SpawnVillagers.redVillager){

				if(e.getDamager() instanceof Player){
					Player p = (Player) e.getDamager();
					if(e.getEntity() == SpawnVillagers.blueVillager){
						if(Util.blueTeam.contains(p.getName())){
							e.setDamage(0.0);
							e.setCancelled(true);
						}else{
							SpawnVillagers.lastBlue = p.getName();
							updateVillager("Blue", e.getDamage());
						}
					}
					if(e.getEntity() == SpawnVillagers.redVillager){
						if(Util.redTeam.contains(p.getName())){
							e.setDamage(0.0);		
							e.setCancelled(true);
						}else{
							SpawnVillagers.lastRed = p.getName();
							updateVillager("Red", e.getDamage());
						}
					}

				}

				if(e.getDamager() instanceof Arrow){
					Arrow a = (Arrow) e.getDamager();
					if(a.getShooter() instanceof Player){
						Player shooter = (Player)a.getShooter();
						if(e.getEntity() == SpawnVillagers.blueVillager){
							if(Util.blueTeam.contains(shooter.getName())){
								e.setDamage(0.0);
								e.setCancelled(true);
							}else{
								SpawnVillagers.lastBlue = shooter.getName();
								updateVillager("Blue", e.getDamage());
							}
						}else
							if(e.getEntity() == SpawnVillagers.redVillager){
								if(Util.redTeam.contains(shooter.getName())){
									e.setDamage(0.0);
									e.setCancelled(true);
								}else{
									SpawnVillagers.lastRed = shooter.getName();
									updateVillager("Red", e.getDamage());
								}
							}
					}
				}
			}

		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onSetHP(EntityDamageEvent e){
		if(e.getEntity() == SpawnVillagers.blueVillager){
			if(SpawnVillagers.lastBlueAttack > 10){
				Util.broadcastGlobalMessage(Util.blueUnderAttack);
				SpawnVillagers.lastBlueAttack = 0;
			}
			e.getEntity().getWorld().playSound(e.getEntity().getLocation(), Sound.VILLAGER_HIT, 1f, 1f);
			e.setCancelled(true);
			if(((Damageable) SpawnVillagers.bv).getHealth() > e.getDamage()){
				SpawnVillagers.bv.setHealth( ((Damageable)SpawnVillagers.bv).getHealth() - e.getDamage());
			}else{
				SpawnVillagers.bv.setHealth(0.0);	
			}

		}else
			if(e.getEntity() == SpawnVillagers.redVillager){
				if(SpawnVillagers.lastRedAttack > 10){
					Util.broadcastGlobalMessage(Util.redUnderAttack);
					SpawnVillagers.lastRedAttack = 0;
				}
				e.getEntity().getWorld().playSound(e.getEntity().getLocation(), Sound.VILLAGER_HIT, 1f, 1f);
				if(((Damageable) SpawnVillagers.rv).getHealth() > e.getDamage()){
					e.setCancelled(true);
					SpawnVillagers.rv.setHealth( ((Damageable)SpawnVillagers.rv).getHealth() - e.getDamage());
				}else{
					SpawnVillagers.bv.setHealth(0.0);	
				}
			}
	}

	public static void updateVillager(String team, double d){
		switch (team){
		case "Red":
			if(SpawnVillagers.rv.isValid()){
				if(((Damageable)SpawnVillagers.rv).getHealth() > d){
					SpawnVillagers.rv.setHealth(SpawnVillagers.redHP-d);
					SpawnVillagers.redHP = SpawnVillagers.redHP-d;
				}else{
					SpawnVillagers.rv.remove();
					if(SpawnVillagers.lastRed != null){
						Util.broadcastGlobalMessage(Util.villagerRedDeathMSG.replace("%killer%", SpawnVillagers.lastRed));
					}else{
						Util.broadcastGlobalMessage(Util.villagerRedDeathMSG.replace("%killer%", Util.villagerUnknownDeath));
					}
					SpawnVillagers.redHP = SpawnVillagers.redHP-d;
				}
			}
			break;
		case "Blue":
			if(SpawnVillagers.bv.isValid()){
				if(((Damageable)SpawnVillagers.bv).getHealth() > d){
					SpawnVillagers.bv.setHealth(SpawnVillagers.blueHP-d);
					SpawnVillagers.blueHP = SpawnVillagers.blueHP-d;
				}else{
					SpawnVillagers.bv.remove();
					if(SpawnVillagers.lastBlue != null){
						Util.broadcastGlobalMessage(Util.villagerBlueDeathMSG.replace("%killer%", SpawnVillagers.lastBlue));
					}else{
						Util.broadcastGlobalMessage(Util.villagerBlueDeathMSG.replace("%killer%", Util.villagerBlueBarMSG));
					}
					SpawnVillagers.blueHP = SpawnVillagers.blueHP-d;
				}
			}
			break;
		}

		BossBarClass.updateBars(SpawnVillagers.redHP, SpawnVillagers.blueHP);
	}

	@EventHandler
	public void onFallOtherDMG(final EntityDamageEvent e){

		if(Util.gameInProgress){

			if(e.isCancelled()==false){

				if(e.getCause() != DamageCause.ENTITY_ATTACK
						&& e.getCause() != DamageCause.PROJECTILE){

					// If it's our Villager
					if(e.getEntity() == SpawnVillagers.blueVillager || e.getEntity() == SpawnVillagers.redVillager){

						if(e.getCause() == DamageCause.BLOCK_EXPLOSION){

							Bukkit.getScheduler().scheduleSyncDelayedTask(pl, new Runnable() {
								public void run(){	

									@SuppressWarnings("unchecked")
									HashMap<Location, String> clone = (HashMap<Location, String>) KitNebulanaut.locs.clone();

									for (Map.Entry<Location, String> entry : clone.entrySet()) {
										Location loc = entry.getKey();
										String shooter = entry.getValue();

										if(e.getEntity().getLocation().distance(loc) <= 5){

											if(e.getEntity() == SpawnVillagers.redVillager){
												if(Util.redTeam.contains(shooter)){

													e.setDamage(0.0);
													e.setCancelled(true);
												}else{
													updateVillager("Red", e.getDamage());
												}
											}else
												if(e.getEntity() == SpawnVillagers.blueVillager){
													if(Util.blueTeam.contains(shooter)){
														e.setDamage(0.0);
														e.setCancelled(true);
													}else{
														updateVillager("Blue", e.getDamage());
													}
												}


										}
									}
								}

							},5L);
						}else

							if(e.getDamage() > 0){
								if(e.getEntity() == SpawnVillagers.blueVillager){
									updateVillager("Blue", e.getDamage());
								}
								if(e.getEntity() == SpawnVillagers.redVillager){
									updateVillager("Red", e.getDamage());
								}
							}

						BossBarClass.updateBars(SpawnVillagers.redHP, SpawnVillagers.blueHP);

					}
				}
			}
		}

	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onKill(EntityDeathEvent e){

		if(Util.gameInProgress){

			// If it's our villager
			if(e.getEntity() == SpawnVillagers.blueVillager || e.getEntity() == SpawnVillagers.redVillager){
				Entity v =  e.getEntity();

				if(v == SpawnVillagers.blueVillager){
					BossBarClass.updateBars(SpawnVillagers.redHP, 0.0);
					if(e.getEntity().getKiller() instanceof Player){
						Util.broadcastGlobalMessage(Util.villagerBlueDeathMSG.replace("%killer%", e.getEntity().getKiller().getName()));
					}else
						if(e.getEntity().getKiller() instanceof Arrow){
							Arrow a = (Arrow)e.getEntity();
							if(a.getShooter() instanceof Player){
								Player shooter = (Player)a.getShooter();
								Util.broadcastGlobalMessage(Util.villagerBlueDeathMSG.replace("%killer%", shooter.getName()));
							}else{
								Util.broadcastGlobalMessage(Util.villagerRedDeathMSG.replace("%killer%", Util.villagerUnknownDeath));
							}
						}else{
							Util.broadcastGlobalMessage(Util.villagerRedDeathMSG.replace("%killer%", Util.villagerUnknownDeath));
						}
					pl.gamestop.gameStop();
				}else
					if(v == SpawnVillagers.redVillager){
						BossBarClass.updateBars(0.0, SpawnVillagers.blueHP);
						if(e.getEntity().getKiller() instanceof Player){

							Util.broadcastGlobalMessage(Util.villagerRedDeathMSG.replace("%killer%", e.getEntity().getKiller().getName()));
						}else
							if(e.getEntity().getKiller() instanceof Arrow){
								Arrow a = (Arrow)e.getEntity();
								if(a.getShooter() instanceof Player){
									Player shooter = (Player)a.getShooter();
									Util.broadcastGlobalMessage(Util.villagerBlueDeathMSG.replace("%killer%", shooter.getName()));
								}else{
									Util.broadcastGlobalMessage(Util.villagerRedDeathMSG.replace("%killer%", Util.villagerUnknownDeath));
								}
							}else{
								Util.broadcastGlobalMessage(Util.villagerRedDeathMSG.replace("%killer%", Util.villagerUnknownDeath));
							}
					}
			}
		}
	}

	@EventHandler
	public void onOpenVillager(PlayerInteractEntityEvent e){

		if(e.getRightClicked() instanceof Villager){
			if(Util.gameInProgress){
				e.setCancelled(true);
			}
		}
	}

}
