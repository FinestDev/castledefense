package me.FinestDev.CastleDefense.Items;

import java.util.ArrayList;

import me.FinestDev.CastleDefense.Kits.Kits;
import me.FinestDev.CastleDefense.Utilities.Util;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class Items {

	public static ItemStack shears;
	public static ItemStack compass;
	public static ItemStack menu;

	// Armors
	public static ItemStack rhelm;
	public static ItemStack rchest;
	public static ItemStack rlegg;
	public static ItemStack rboots;

	public static ItemStack bhelm;
	public static ItemStack bchest;
	public static ItemStack blegg;
	public static ItemStack bboots;
	
	public static ItemStack rglass;
	public static ItemStack bglass;
	
	// Nebulanaut
	public static ItemStack NEstonesword;
	public static ItemStack NEevasive;
	
	// Wizard
	public static ItemStack chainChest;
	public static ItemStack chainBoots;
	public static ItemStack wizardStaff;
	public static ItemStack wizardWand;
	
	// Nebulax
	public static ItemStack nebulaxBlade;
	public static ItemStack nebulaxBomb;
	public static ItemStack sonicBoom;
	
	public static ItemStack dhelm;
	public static ItemStack dchest;
	public static ItemStack dlegg;
	public static ItemStack dboots;

	public static void setupItems(){
		shears = new ItemStack(Material.SHEARS, 1);
		shears.addEnchantment(Enchantment.DIG_SPEED, 5);
		shears.addUnsafeEnchantment(Enchantment.DURABILITY, 5);

		compass = new ItemStack(Material.COMPASS, 1);
		
		rglass = new ItemStack(Material.STAINED_GLASS, 1, (byte) 14);
		bglass = new ItemStack(Material.STAINED_GLASS, 1, (byte) 11);
		
		chainChest = new ItemStack(Material.CHAINMAIL_CHESTPLATE, 1);
		chainBoots = new ItemStack(Material.CHAINMAIL_BOOTS, 1);
		
		menu = new ItemStack(Material.CHEST, 1);
		ItemMeta menuMeta = menu.getItemMeta();
		menuMeta.setDisplayName(Util.menuName);
		menu.setItemMeta(menuMeta);
		
		// Menu Icons etc.
		ItemStack nebulanaut = new ItemStack(Material.ENDER_PEARL, 1);
		ItemMeta nebulanautMeta = nebulanaut.getItemMeta();
		ArrayList<String> nebulauntLore = new ArrayList<String>();
		nebulauntLore.add("�rWeapons�7:");
		nebulauntLore.add("�f- �7�lStone �8Sword");
		nebulauntLore.add("");
		nebulauntLore.add("�rSpecial Abilities�7:");
		nebulauntLore.add("�f- �4�lDouble �cJump");
		nebulauntLore.add("�f- �6�lBlast �r�eOff");
		nebulanautMeta.setLore(nebulauntLore);
		nebulanautMeta.setDisplayName("�b�lNebula�9Naut");
		nebulanaut.setItemMeta(nebulanautMeta);
		
		ItemStack wizard = new ItemStack(Material.BLAZE_ROD, 1);
		ItemMeta wizardMeta = wizard.getItemMeta();
		ArrayList<String> wizardLore = new ArrayList<String>();
		wizardLore.add("�rWeapons�7:");
		wizardLore.add("�f- �e�lWizard �r�6Staff");
		wizardLore.add("");
		wizardLore.add("�rSpecial Abilities�7:");
		wizardLore.add("�f- �4�lExplosive �r�cWand");
		wizardLore.add("�f- �9�lEnder �r�bPearl");
		wizardMeta.setLore(wizardLore);
		wizardMeta.setDisplayName("�6�lWizard");
		wizard.setItemMeta(wizardMeta);
		
		ItemStack nebulax = new ItemStack(Material.NETHER_STAR, 1);
		ItemMeta nebulaxMeta = nebulax.getItemMeta();
		ArrayList<String> nebulaxLore = new ArrayList<String>();
		nebulaxLore.add("�4�lVIP �r�cKit:");
		nebulaxLore.add("�f- �6MCNebulas.com/�estore");
		nebulaxLore.add("�rWeapons�7:");
		nebulaxLore.add("�f- �b�lNebulax �r�9Blade");
		nebulaxLore.add("");
		nebulaxLore.add("�rSpecial Abilities�7:");
		nebulaxLore.add("�f- �4�lExplosive �cBall");
		nebulaxLore.add("�f- �1�lSonic �r�bBoom");
		nebulaxMeta.setLore(nebulaxLore);
		nebulaxMeta.setDisplayName("�9�lNe�b�lbu�9�llax");
		nebulax.setItemMeta(nebulaxMeta);

		// &7[Member] &8Kits &bVIP &8Kits
		ItemStack gplane = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 5);
		ItemMeta gplaneMeta = gplane.getItemMeta();
		gplaneMeta.setDisplayName("�7[Member] �8Kits");
		gplane.setItemMeta(gplaneMeta);
		
		ItemStack bplane = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 3);
		ItemMeta bplaneMeta = bplane.getItemMeta();
		bplaneMeta.setDisplayName("�b[VIP] �8Kits");
		bplane.setItemMeta(bplaneMeta);
		
		
		Kits.kitMenu.clear();
		Kits.kitMenu.setItem(0, gplane);
		Kits.kitMenu.setItem(1, nebulanaut);
		Kits.kitMenu.setItem(2, wizard);
		Kits.kitMenu.setItem(3, gplane);
		Kits.kitMenu.setItem(4, bplane);
		Kits.kitMenu.setItem(5, bplane);
		Kits.kitMenu.setItem(6, nebulax);
		Kits.kitMenu.setItem(7, bplane);
		Kits.kitMenu.setItem(8, bplane);
		
		// Nebulanaut
		NEstonesword = new ItemStack(Material.STONE_SWORD, 1);
		NEstonesword.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 2);
		NEstonesword.addUnsafeEnchantment(Enchantment.KNOCKBACK, 1);
		NEstonesword.addUnsafeEnchantment(Enchantment.DURABILITY, 3);
		
		NEevasive = new ItemStack(Material.INK_SACK, 1, (byte) 14);
		ItemMeta NEevasiveMETA = NEevasive.getItemMeta();
		NEevasiveMETA.setDisplayName("Blast Off");
		NEevasive.setItemMeta(NEevasiveMETA);
		
		// Wizard
		wizardStaff = new ItemStack(Material.STICK, 1);
		ItemMeta wizardStaffMeta = wizardStaff.getItemMeta();
		wizardStaffMeta.setDisplayName("Wizard Staff");
		wizardStaff.setItemMeta(wizardStaffMeta);
		
		wizardStaff.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 3);
		wizardStaff.addUnsafeEnchantment(Enchantment.KNOCKBACK, 2);

		wizardWand = new ItemStack(Material.BLAZE_ROD, 1);
		ItemMeta wizardWandMeta = wizardWand.getItemMeta();
		wizardWandMeta.setDisplayName("Wizard Wand");
		wizardWand.setItemMeta(wizardWandMeta);
		
		// Nebulax
		nebulaxBlade = new ItemStack(Material.NETHER_STAR, 1);
		nebulaxBlade.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 2);
		nebulaxBlade.addUnsafeEnchantment(Enchantment.KNOCKBACK, 1);
		
		ItemMeta nebulaxBladeMeta = nebulaxBlade.getItemMeta();
		nebulaxBladeMeta.setDisplayName("Nebulax Blade");
		nebulaxBlade.setItemMeta(nebulaxBladeMeta);
		
		sonicBoom = new ItemStack(Material.INK_SACK, 1, (byte) 6);
		ItemMeta sonicBoomMeta = sonicBoom.getItemMeta();
		sonicBoomMeta.setDisplayName("Sonic Boom");
		sonicBoom.setItemMeta(sonicBoomMeta);
		
		nebulaxBomb = new ItemStack(Material.SNOW_BALL, 1);
		
		dhelm = new ItemStack(Material.DIAMOND_HELMET, 1);
		
		dchest = new ItemStack(Material.DIAMOND_CHESTPLATE, 1);
		dlegg = new ItemStack(Material.DIAMOND_LEGGINGS, 1);
		dboots = new ItemStack(Material.DIAMOND_BOOTS, 1);
		
		
		rhelm = new ItemStack(Material.LEATHER_HELMET, 1);
		rchest = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
		rlegg = new ItemStack(Material.LEATHER_LEGGINGS, 1);
		rboots = new ItemStack(Material.LEATHER_BOOTS, 1);
		
		bhelm = new ItemStack(Material.LEATHER_HELMET, 1);
		bchest = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
		blegg = new ItemStack(Material.LEATHER_LEGGINGS, 1);
		bboots = new ItemStack(Material.LEATHER_BOOTS, 1);
		
		// Red leather
		LeatherArmorMeta ram = (LeatherArmorMeta)rhelm.getItemMeta();
		ram.setColor(Color.fromRGB(255, 0, 0));
		rhelm.setItemMeta(ram);
		rchest.setItemMeta(ram);
		rlegg.setItemMeta(ram);
		rboots.setItemMeta(ram);
		
		// Blue leather
		LeatherArmorMeta bam = (LeatherArmorMeta)bhelm.getItemMeta();
		bam.setColor(Color.fromRGB(0, 0, 255));
		bhelm.setItemMeta(bam);
		bchest.setItemMeta(bam);
		blegg.setItemMeta(bam);
		bboots.setItemMeta(bam);
	}

}
